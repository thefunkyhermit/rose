# The Gilded Rose

## Approach

##### Endpoints
This application uses 2 main endpoints to allow customers to view and purchase items via the API;
1) GET /items - this is used to get inventory

2) POST /purchases - this is used to make purchases.

##### Datastore
A few simple in memory collections were used to perform database persistence.  This was done for ease of implementation.  Normally I'd use some sort of datastore to allow for persistence, scalability, recovery, etc.

##### Surge Pricing
This was achived by recording the Guava LoadingCache class.  Rather than having some kind of polling process checking on a history of events, we maintain this class instead which handles a TTL on fields.  This way we only need to check the size of the LoadingCache to determine if surge pricing applies.

##### Authentication

A simple Basic Authentication was build in order to handle Authentication.  Usernames and passwords are store in an in memory collection.  The id and password is then passed to the endpoint in a header with the key `Authentication`.  Normally I'd consider implementing a [ZUUL gateway](https://github.com/Netflix/zuul) to perform auth for us as it separates auth in a different working applicaton from the running applicaton itself (AWS ALB w/ Lambda is also an option now).  The value of the header is also passed as BASE64.  A valid head is `eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9`.

##### Testing
Unit tests using Mockito for mocking were done.  Integration tests were also created; `ItemsIntegrationTest` and `PurchaseIntegrationTest`.

To test the /items end point maually run
```
curl -X GET \
  http://localhost:8080/items \
  -H 'authorization: eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
  ````
  
  To make a purchase, run
  
```
curl -X POST \
  http://localhost:8080/purchases \
  -H 'authorization: eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{"id":"bigger thing"}'
  ```
---------------------------------------

### To Run:
The application runs in [Docker](https://www.docker.com/).

To start the application run the following commands in the root direction in this order: 
1) ``./gradlew clean build docker``
2)  ``docker run -p 8080:8080 --name rose-api -t springio/gs-spring-boot-docker``

### TO Stop:
1) ```docker container stop rose-api```

---------------------------------------

#### Cases

GET /ITEMS

Case1
Given: the item table has records
When: The GET api is called
Then: an array containing the records in the database are returned by the endpoint

Case 2
Given: the item table is empty
When: the GET api is called
Then: an empty array is returned by the end endpoint

Case 3
Given: the item table has records
When: An invalid Authentication Header is submitted
Then: An HTTP 401 status is returned

POST /purchases

Case 4
Given: a user attempts to purchase an item
When: An invalid Authentication Header is submitted
Then: the endpoint will return an HTTP status of 401

Case 5
Given: a user attempts to purchase an item
When: the item is not in the database
Then: the endpoint will return an HTTP status of 400

Case 6
Given: a user attempts to purchase an item
When: the item is in the database
Then: the item will be marked as unavailable in the database
  and the endpoint will set the location header to the purchase record in the API
  and the end point will return an HTTP status of 201

Case 7
Given: a user attempts to purchase an item
When: an item has been 10 or more times in the last hour
Then: the item will now cost item.price * 110%

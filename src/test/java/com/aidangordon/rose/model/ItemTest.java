package com.aidangordon.rose.model;

import org.junit.Assert;
import org.junit.Test;

public class ItemTest {


    @Test
    public void testGetName_notEmpty() {
        Item item = new Item ("thing", "it's a thing", 1212);
        Assert.assertEquals("thing", item.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetName_shouldThrowExceptionWhenEmpty() {
        new Item("", "ioinsgoio", 1232);
    }

    @Test
    public void testGetDescription_notEmpty() {
        Item item = new Item ("thing", "it's a thing", 1212);
        Assert.assertEquals("it's a thing", item.getDescription());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDescription_shouldThrowExceptionWhenEmpty() {
        new Item("oinon", "", 13241);
    }

    @Test
    public void testGetPrice_greaterThanZero() {
        Item item = new Item ("thing", "it's a thing", 1212);
        Assert.assertEquals(1212, item.getPrice());
    }

    @Test
    public void testGetPrice_equalToZero() {
        Item item = new Item ("thing", "it's a thing", 0);
        Assert.assertEquals(0, item.getPrice());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPrice_shouldThrowExceptionWhenLessThanZero() {
       new Item ("thing", "it's a thing", -123);
    }

    @Test
    public void testObjectEquals_shouldBeNotEqual() {
        Item item1 = new Item("name 1", "description 1", 1);
        Item item2 = new Item("name 2", "description 2", 2);
        Assert.assertNotEquals(item1, item2);
    }


    @Test
    public void testObjectEquals_shouldBeEqual() {
        Item item1 = new Item("name 1", "description 1", 1);
        Item item2 = new Item("name 1", "description 1", 1 );
        Assert.assertEquals(item1, item2);
    }
}

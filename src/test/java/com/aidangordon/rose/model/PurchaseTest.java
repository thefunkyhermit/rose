package com.aidangordon.rose.model;

import org.junit.Assert;
import org.junit.Test;

public class PurchaseTest {
    @Test
    public void testEqualsPurchase() {
        Purchase purchase1 = new Purchase(new Item("thing", "it's a thing", 99));
        Purchase purchase2 = new Purchase(new Item("thing", "it's a thing", 99));
        Assert.assertEquals(purchase1, purchase2);

    }
}

package com.aidangordon.rose.dao;

import org.junit.Assert;
import org.junit.Test;
import com.aidangordon.rose.model.Item;
import com.aidangordon.rose.model.Purchase;

import java.util.ArrayList;
import java.util.List;

public class PurchaseDaoTest {
    
    @Test
    public void testMakePurchase() {
        PurchaseDao purchaseDao = new PurchaseDao();
        purchaseDao.makePurchase(new Purchase(new Item("thing", "it's a thing", 99)));
        List<Purchase> expectedPurchases = new ArrayList<>();
        expectedPurchases.add(new Purchase(new Item("thing", "it's a thing", 99)));
        Assert.assertEquals(expectedPurchases, purchaseDao.getPurchases());
    }
}

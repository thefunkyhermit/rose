package com.aidangordon.rose.dao;

import org.junit.Assert;
import org.junit.Test;
import com.aidangordon.rose.exception.ItemDoesNotExistException;
import com.aidangordon.rose.model.Item;

import java.util.HashMap;
import java.util.Map;

public class ItemDaoTest {
    @Test
    public void testGetItems() {
        Map<String, Item> items = new HashMap<>();
        items.put("thing", new Item("thing", "just a thing", 99));
        items.put("bigger thing", new Item("bigger thing", "thing but bigger than a regular thing", 199));
        items.put("biggest thing", new Item("biggest thing", "the biggest thing of them all", 999));
        ItemDao itemDao = new ItemDao();
        Assert.assertEquals(items, itemDao.getItems());
    }

    @Test
    public void testRemoveItem_itemInList() throws ItemDoesNotExistException {
        ItemDao itemDao = new ItemDao();
        itemDao.removeItem("thing");

        Map<String, Item> expectedItems = new HashMap<>();
        expectedItems.put("bigger thing", new Item("bigger thing", "thing but bigger than a regular thing", 199));
        expectedItems.put("biggest thing", new Item("biggest thing", "the biggest thing of them all", 999));

        Assert.assertEquals(expectedItems, itemDao.getItems());
    }
}

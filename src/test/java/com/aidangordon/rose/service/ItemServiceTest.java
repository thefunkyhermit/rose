package com.aidangordon.rose.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.aidangordon.rose.dao.ItemDao;
import com.aidangordon.rose.model.Item;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

public class ItemServiceTest {

    @Mock
    private ItemDao itemDao;

    @InjectMocks
    private ItemService itemService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetItems() {
        Map<String, Item> items = new HashMap<>();
        items.put("thing", new Item("thing", "just a thing", 99));
        items.put("bigger thing", new Item("bigger thing", "thing but bigger than a regular thing", 199));
        items.put("biggest thing", new Item("biggest thing", "the biggest thing of them all", 999));

        when(itemDao.getItems()).thenReturn(items);
        Assert.assertEquals(items, itemService.getItems());
    }

}

package com.aidangordon.rose.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.stereotype.Component;
import com.aidangordon.rose.dao.AuthDao;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Component
public class AuthServiceTest {

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Mock
    private AuthDao authDao;

    @InjectMocks
    private AuthService authService;

    @Test
    public void testIsAuthenticated() throws IOException {
        when(authDao.getAuthRecord(any(String.class))).thenReturn("thisisasecret");
        Assert.assertTrue(authService.isAuthenticated("eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9"));
    }

    @Test
    public void testIsAuthenticated_badBase64() throws IOException {
        Assert.assertFalse(authService.isAuthenticated("oinoinoin"));
    }

    @Test
    public void testIsAuthenticated_badSecret() throws IOException {
        when(authDao.getAuthRecord(any(String.class))).thenReturn("secret");
        Assert.assertFalse(authService.isAuthenticated("eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoiYmFkc2VjcmV0In0="));
    }

    @Test
    public void testIsAuthenticated_userDoesNotExist() throws IOException {
        when(authDao.getAuthRecord(any(String.class))).thenReturn(null);
        Assert.assertFalse(authService.isAuthenticated("eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoiYmFkc2VjcmV0In0="));
    }
}

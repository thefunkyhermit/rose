package com.aidangordon.rose.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.aidangordon.rose.dao.ItemDao;
import com.aidangordon.rose.dao.PurchaseDao;
import com.aidangordon.rose.exception.ItemDoesNotExistException;
import com.aidangordon.rose.model.Item;
import com.aidangordon.rose.model.Purchase;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PurchaseServiceTest {
    @Mock
    private ItemDao itemDao;

    @Mock
    private PurchaseDao purchaseDao;

    @InjectMocks
    private PurchaseService purchaseService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMakePurchase() throws ItemDoesNotExistException {
        Item thing = new Item("thing", "it's a thing", 99);
        when(itemDao.removeItem(any(String.class))).thenReturn(thing);
        purchaseService.makePurchase("thing");

        //Make sure that the same object returned from the dao is sent to the makepurchase call
        verify(purchaseDao).makePurchase(new Purchase(thing));
        List<Purchase> expectedPurchases = new ArrayList<>();
        expectedPurchases.add(new Purchase(new Item("thing", "it's a thing", 99)));

        List<Purchase> mockedPurchases = new ArrayList<>();
        mockedPurchases.add(new Purchase(new Item("thing", "it's a thing", 99)));

        when(purchaseDao.getPurchases()).thenReturn(mockedPurchases);
        Assert.assertEquals(expectedPurchases, purchaseService.getPurchases());
    }

    @Test(expected = ItemDoesNotExistException.class)
    public void testMakePurchase_ItemNotAvailable() throws ItemDoesNotExistException {
        when(itemDao.removeItem(any(String.class))).thenThrow(ItemDoesNotExistException.class);
        purchaseService.makePurchase("thing");
    }
}

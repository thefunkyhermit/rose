package com.aidangordon.rose.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PurchaseIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void makePurchase_shouldHaveOneLessItem() {
        //First verify there are 3 items in the DB
        ResponseEntity<Map> itemsResponse = getItems();
        Assert.assertEquals(itemsResponse.getBody().size(), 3);

        //Now make the purchase
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9");

        PurchasePostBody purchasePostBody = new PurchasePostBody();
        purchasePostBody.setId("thing");
        HttpEntity<PurchasePostBody> request = new HttpEntity<>(purchasePostBody, headers);

        ResponseEntity response = restTemplate.exchange("/purchases", HttpMethod.POST, request, Map.class);

        Assert.assertEquals(HttpStatus.CREATED.value(), response.getStatusCodeValue());

        //Now verify we have 1 less item available in the store
        ResponseEntity<Map> itemsResponse2 = getItems();
        Assert.assertEquals(itemsResponse2.getBody().size(), 2);
    }

    @Test
    public void makePurchase_shouldReturn404WhenItemDoesNotExist() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9");

        PurchasePostBody purchasePostBody = new PurchasePostBody();
        purchasePostBody.setId("notathigng");
        HttpEntity<PurchasePostBody> request = new HttpEntity<>(purchasePostBody, headers);

        ResponseEntity response = restTemplate.exchange("/purchases", HttpMethod.POST, request, Map.class);

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCodeValue());

    }

    private ResponseEntity<Map> getItems() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9");

        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
    }
}

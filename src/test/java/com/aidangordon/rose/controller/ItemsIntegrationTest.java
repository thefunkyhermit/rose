package com.aidangordon.rose.controller;


import com.aidangordon.rose.model.Item;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ItemsIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getItems_noAuthHeader() {
        Map<String, Item> response = restTemplate.getForObject("/items", Map.class);

        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(), response.get("status"));
    }

    @Test
    public void getItems_BadAuthHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "aeyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9");

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map> response = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);

        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCodeValue());
    }

    @Test
    public void createClient_getItemsAuthorized() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9");

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map> response = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);

        Map<String, Item> expectedItems = new LinkedHashMap<>();
        expectedItems.put("thing", new Item("thing", "just a thing", 99));
        expectedItems.put("bigger thing", new Item("bigger thing", "thing but bigger than a regular thing", 199));
        expectedItems.put("biggest thing", new Item("biggest thing", "the biggest thing of them all", 999));

        Assert.assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());

        Map actualThing = (Map) response.getBody().get("thing");
        Item expectedThing = expectedItems.get("thing");
        Assert.assertEquals(expectedThing.getName(), actualThing.get("name"));
        Assert.assertEquals(expectedThing.getDescription(), actualThing.get("description"));
        Assert.assertEquals(expectedThing.getPrice(), actualThing.get("price"));

        Map actualBiggerthing = (Map) response.getBody().get("bigger thing");
        Item expectedBiggerThing = expectedItems.get("bigger thing");
        Assert.assertEquals(expectedBiggerThing.getName(), actualBiggerthing.get("name"));
        Assert.assertEquals(expectedBiggerThing.getDescription(), actualBiggerthing.get("description"));
        Assert.assertEquals(expectedBiggerThing.getPrice(), actualBiggerthing.get("price"));

        Map actualBiggestThing = (Map) response.getBody().get("biggest thing");
        Item expectedBiggestThing = expectedItems.get("biggest thing");
        Assert.assertEquals(expectedBiggestThing.getName(), actualBiggestThing.get("name"));
        Assert.assertEquals(expectedBiggestThing.getDescription(), actualBiggestThing.get("description"));
        Assert.assertEquals(expectedBiggestThing.getPrice(), actualBiggestThing.get("price"));
    }

    @Test
    public void createClient_shouldReturnSurgePricingAfter10tries() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "eyJ1c2VySWQiOiJ0ZXN0Iiwic2VjcmV0IjoidGhpc2lzYXNlY3JldCJ9");

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map> response = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);

        Map<String, Item> expectedItems = new LinkedHashMap<>();
        expectedItems.put("thing", new Item("thing", "just a thing", 99));
        expectedItems.put("bigger thing", new Item("bigger thing", "thing but bigger than a regular thing", 199));
        expectedItems.put("biggest thing", new Item("biggest thing", "the biggest thing of them all", 999));

        Assert.assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());

        checkResultsBeforeSurge(response, expectedItems);

        ResponseEntity<Map> response2 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response2.getStatusCodeValue());
        checkResultsBeforeSurge(response2, expectedItems);

        ResponseEntity<Map> response3 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response2.getStatusCodeValue());
        checkResultsBeforeSurge(response3, expectedItems);

        ResponseEntity<Map> response4 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response4.getStatusCodeValue());
        checkResultsBeforeSurge(response4, expectedItems);

        ResponseEntity<Map> response5= restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response5.getStatusCodeValue());
        checkResultsBeforeSurge(response5, expectedItems);

        ResponseEntity<Map> response6 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response6.getStatusCodeValue());
        checkResultsBeforeSurge(response6, expectedItems);

        ResponseEntity<Map> response7 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response7.getStatusCodeValue());
        checkResultsBeforeSurge(response7, expectedItems);

        ResponseEntity<Map> response8 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response8.getStatusCodeValue());
        checkResultsBeforeSurge(response8, expectedItems);

        ResponseEntity<Map> response9 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response9.getStatusCodeValue());
        checkResultsBeforeSurge(response9, expectedItems);

        ResponseEntity<Map> response10 = restTemplate.exchange("/items", HttpMethod.GET, entity, Map.class);
        Assert.assertEquals(HttpStatus.OK.value(), response10.getStatusCodeValue());
        checkResultsAfterSurge(response10, expectedItems);

    }

    //TODO - write a test that checks for surge pricing ending after the time period.

    private void checkResultsAfterSurge(ResponseEntity<Map> response, Map<String, Item> expectedItems) {
        Map actualThing = (Map) response.getBody().get("thing");
        Assert.assertEquals(108, actualThing.get("price"));

        Map actualBiggerthing = (Map) response.getBody().get("bigger thing");
        Assert.assertEquals(218, actualBiggerthing.get("price"));

        Map actualBiggestThing = (Map) response.getBody().get("biggest thing");
        Assert.assertEquals(1098, actualBiggestThing.get("price"));
    }

    private void checkResultsBeforeSurge(ResponseEntity<Map> response, Map<String, Item> expectedItems) {
        Map actualThing = (Map) response.getBody().get("thing");
        Item expectedThing = expectedItems.get("thing");
        Assert.assertEquals(expectedThing.getPrice(), actualThing.get("price"));

        Map actualBiggerthing = (Map) response.getBody().get("bigger thing");
        Item expectedBiggerThing = expectedItems.get("bigger thing");
        Assert.assertEquals(expectedBiggerThing.getPrice(), actualBiggerthing.get("price"));

        Map actualBiggestThing = (Map) response.getBody().get("biggest thing");
        Item expectedBiggestThing = expectedItems.get("biggest thing");
        Assert.assertEquals(expectedBiggestThing.getPrice(), actualBiggestThing.get("price"));
    }


}

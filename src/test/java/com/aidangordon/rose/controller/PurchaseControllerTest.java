package com.aidangordon.rose.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.aidangordon.rose.exception.ItemDoesNotExistException;
import com.aidangordon.rose.model.Item;
import com.aidangordon.rose.service.PurchaseService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

public class PurchaseControllerTest {

    @Mock
    private PurchaseService purchaseService;

    @InjectMocks
    private PurchaseController purchaseController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void makePurchase() {
        PurchasePostBody body = new PurchasePostBody();
        body.setId("thing");
        ResponseEntity<Item> returnedItem = purchaseController.makePurchase(body);
        Assert.assertEquals(HttpStatus.CREATED, returnedItem.getStatusCode());
    }

    @Test
    public void makePurchase_ItemDoesNotExist() throws ItemDoesNotExistException {
        PurchasePostBody body = new PurchasePostBody();
        body.setId("thing");
        doThrow(ItemDoesNotExistException.class).when(purchaseService).makePurchase(any(String.class));
        ResponseEntity<Item> response =  purchaseController.makePurchase(body);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

package com.aidangordon.rose.model;

import java.util.Objects;

public class Purchase {
    private final Item item;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Purchase purchase = (Purchase) o;
        return Objects.equals(item, purchase.item);
    }

    @Override
    public int hashCode() {

        return Objects.hash(item);
    }

    public Purchase(Item item) {
        this.item = item;
    }
}

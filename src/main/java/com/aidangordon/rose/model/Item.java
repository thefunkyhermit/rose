package com.aidangordon.rose.model;

import java.util.Objects;

public class Item {
    private final String name;
    private final String description;
    private final int price;

    public Item(String name, String description, int price) {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name is required for an item");
        }
        if (description.isEmpty()) {
            throw new IllegalArgumentException("description is required for an item");
        }
        if (price < 0) {
            throw new IllegalArgumentException("Price must be equal to or greater than 0");
        }
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return price == item.price &&
                Objects.equals(name, item.name) &&
                Objects.equals(description, item.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, price);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }
}

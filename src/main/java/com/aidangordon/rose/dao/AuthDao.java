package com.aidangordon.rose.dao;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class AuthDao {

    private final Map<String, String> secrets = new HashMap<>();
    public AuthDao () {
        secrets.put("test", "thisisasecret");
        secrets.put("test2", "thisisanothersecret");
    }
    public String getAuthRecord(String userId) {
        return secrets.get(userId);
    }

}

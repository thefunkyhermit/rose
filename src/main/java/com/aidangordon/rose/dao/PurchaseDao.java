package com.aidangordon.rose.dao;

import org.springframework.stereotype.Component;
import com.aidangordon.rose.model.Purchase;

import java.util.ArrayList;
import java.util.List;

@Component
public class PurchaseDao {
    private final List<Purchase> purchases = new ArrayList<>();

    public void makePurchase(Purchase purchase) {
        purchases.add(purchase);
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }
}

package com.aidangordon.rose.dao;

import org.springframework.stereotype.Component;
import com.aidangordon.rose.exception.ItemDoesNotExistException;
import com.aidangordon.rose.model.Item;

import java.util.HashMap;
import java.util.Map;

@Component
public class ItemDao {
    private final Map<String, Item> items;

    public ItemDao () {

        items = new HashMap<>();
        items.put("thing", new Item("thing", "just a thing", 99));
        items.put("bigger thing", new Item("bigger thing", "thing but bigger than a regular thing", 199));
        items.put("biggest thing", new Item("biggest thing", "the biggest thing of them all", 999));
    }

    public Map<String, Item> getItems() {
        return items;
    }

    public Item removeItem(String id) throws ItemDoesNotExistException {
        Item item = items.get(id);
        if (item == null) {
            throw new ItemDoesNotExistException(id);
        }
        items.remove(id);
        return item;
    }
}

package com.aidangordon.rose.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.aidangordon.rose.model.Item;
import com.aidangordon.rose.service.ItemService;

import java.util.Map;

@RestController
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping("/items")
    public Map<String, Item> getItems() {
        return itemService.getItems();
    }
}

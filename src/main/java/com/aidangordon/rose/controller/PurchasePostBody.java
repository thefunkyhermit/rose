package com.aidangordon.rose.controller;

class PurchasePostBody {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

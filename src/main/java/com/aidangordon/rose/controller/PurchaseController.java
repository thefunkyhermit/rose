package com.aidangordon.rose.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.aidangordon.rose.exception.ItemDoesNotExistException;
import com.aidangordon.rose.model.Item;
import com.aidangordon.rose.service.PurchaseService;

@RestController
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<Item> makePurchase(@RequestBody PurchasePostBody body) {
        ResponseEntity responseEntity;
        try {
            purchaseService.makePurchase(body.getId());
            responseEntity = new ResponseEntity(HttpStatus.CREATED);
        } catch (ItemDoesNotExistException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

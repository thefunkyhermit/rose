package com.aidangordon.rose.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.aidangordon.rose.dao.ItemDao;
import com.aidangordon.rose.dao.PurchaseDao;
import com.aidangordon.rose.exception.ItemDoesNotExistException;
import com.aidangordon.rose.model.Item;
import com.aidangordon.rose.model.Purchase;

import java.util.List;

@Component
public class PurchaseService {

    @Autowired
    private PurchaseDao purchaseDao;

    @Autowired
    private ItemDao itemDao;

    public void makePurchase(String id) throws ItemDoesNotExistException {
        Item item = itemDao.removeItem(id);
        purchaseDao.makePurchase(new Purchase(item));
    }

    public List<Purchase> getPurchases() {
        return purchaseDao.getPurchases();
    }
}

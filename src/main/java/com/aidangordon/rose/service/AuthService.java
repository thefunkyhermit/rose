package com.aidangordon.rose.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.aidangordon.rose.dao.AuthDao;
import com.aidangordon.rose.model.AuthHeader;

import java.io.IOException;
import java.util.Base64;

@Component
public class AuthService {

    @Autowired
    private AuthDao authDao;

    public boolean isAuthenticated(String authHeader) throws IOException {
        if (authHeader == null) {
            return false;
        }
        byte[] decoded;
        try {
            decoded = Base64.getMimeDecoder().decode(authHeader);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        String output = new String(decoded);
        AuthHeader itemWithOwner;
        try {
            itemWithOwner = new ObjectMapper().readValue(output, AuthHeader.class);
        } catch (JsonParseException exception) {
            return false;
        }
        String secretFromDao = authDao.getAuthRecord(itemWithOwner.userId);
        return secretFromDao != null && secretFromDao.equals(itemWithOwner.secret);
    }
}

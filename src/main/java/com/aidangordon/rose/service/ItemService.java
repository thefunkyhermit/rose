package com.aidangordon.rose.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.aidangordon.rose.dao.ItemDao;
import com.aidangordon.rose.model.Item;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class ItemService {

    @Autowired
    private ItemDao itemDao;

    private final LoadingCache<String, String> cache;

    public ItemService() {

        CacheLoader<String, String> loader;
        loader = new CacheLoader<String, String>() {
            @Override
            public String load(String key) {
                return key.toUpperCase();
            }
        };

        cache = CacheBuilder.newBuilder()
                .expireAfterAccess(1, TimeUnit.HOURS)
                .build(loader);

    }

    public Map<String, Item> getItems() {
        cache.put(java.util.UUID.randomUUID().toString(), "test");
        Map<String, Item> originalItems = itemDao.getItems();
        Map<String, Item> newItems = originalItems;
        if (cache.size() > 10) {
            newItems = updatePrices(originalItems);
        }
        return newItems;
    }

    private Map<String, Item> updatePrices(Map<String, Item> originalItems) {
        Map<String, Item> newItems;
        newItems = new HashMap<>();

        for (Item item : originalItems.values()
                ) {
            newItems.put(item.getName(), new Item(item.getName(), item.getDescription(), new Double(item.getPrice() * 1.1d).intValue()));
        }
        return newItems;
    }

}

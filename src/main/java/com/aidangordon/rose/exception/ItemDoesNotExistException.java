package com.aidangordon.rose.exception;

public class ItemDoesNotExistException extends Throwable {
    public ItemDoesNotExistException(String id) {
        super("Id does not exist: " + id);
    }
}
